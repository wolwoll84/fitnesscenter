package com.ksy.fitnesscenter.repository;

import com.ksy.fitnesscenter.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}

package com.ksy.fitnesscenter.repository;

import com.ksy.fitnesscenter.entity.ReadingManager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReadingManagerRepository extends JpaRepository<ReadingManager, Long> {
}

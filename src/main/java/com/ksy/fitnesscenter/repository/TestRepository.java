package com.ksy.fitnesscenter.repository;

import com.ksy.fitnesscenter.entity.Test;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestRepository extends JpaRepository<Test, Long> {
}

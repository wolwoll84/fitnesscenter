package com.ksy.fitnesscenter.controller;

import com.ksy.fitnesscenter.model.ReadingManagerItem;
import com.ksy.fitnesscenter.model.ReadingManagerRequest;
import com.ksy.fitnesscenter.model.ReadingManagerResponse;
import com.ksy.fitnesscenter.service.ReadingManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/reading")
public class ReadingManagerController {
    private final ReadingManagerService readingManagerService;

    @PostMapping("/data")
    public String setData(@RequestBody @Valid ReadingManagerRequest request) {
        readingManagerService.setData(request.getTitle(),
                                      request.getAuthor(),
                                      request.getPublisher(),
                                      request.getGenre(),
                                      request.getDateStarted(),
                                      request.getReview()
                );

        return "등록";
    }

    @GetMapping("/all")
    public List<ReadingManagerItem> getList() {
        List<ReadingManagerItem> result = readingManagerService.getList();
        return result;
    }

    @GetMapping("/book/id/{id}")
    public ReadingManagerResponse getBook(@PathVariable long id) {
        ReadingManagerResponse result = readingManagerService.getBook(id);
        return result;
    }
}

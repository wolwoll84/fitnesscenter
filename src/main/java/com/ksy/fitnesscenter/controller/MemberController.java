package com.ksy.fitnesscenter.controller;

import com.ksy.fitnesscenter.model.MemberItem;
import com.ksy.fitnesscenter.model.MemberRequest;
import com.ksy.fitnesscenter.model.MemberResponse;
import com.ksy.fitnesscenter.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/data")
    public String setData(@RequestBody @Valid MemberRequest request) {
        memberService.setMember(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        List<MemberItem> result = memberService.getMembers();
        return result;
    }

    @GetMapping("/data/id/{id}")
    public MemberResponse getMember(@PathVariable long id) {
        MemberResponse result = memberService.getMember(id);
        return result;
    }
}

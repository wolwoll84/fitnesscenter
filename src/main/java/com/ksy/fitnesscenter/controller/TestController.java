package com.ksy.fitnesscenter.controller;

import com.ksy.fitnesscenter.model.TestItem;
import com.ksy.fitnesscenter.model.TestRequest;
import com.ksy.fitnesscenter.model.TestResponse;
import com.ksy.fitnesscenter.service.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/test")
public class TestController {
    private final TestService testService;

    @PostMapping("/data")
    public String setData(@RequestBody @Valid TestRequest request) {
        testService.setStudent(request);

        return "정상처리";
    }

    @GetMapping("/all")
    public List<TestItem> getStudents() {
        List<TestItem> result = testService.getStudents();
        return result;
    }

    @GetMapping("/data/id/{id}")
    public TestResponse getStudent(@PathVariable long id) {
        TestResponse result = testService.getStudent(id);
        return result;
    }
}

package com.ksy.fitnesscenter.service;

import com.ksy.fitnesscenter.entity.ReadingManager;
import com.ksy.fitnesscenter.model.ReadingManagerItem;
import com.ksy.fitnesscenter.model.ReadingManagerResponse;
import com.ksy.fitnesscenter.repository.ReadingManagerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ReadingManagerService {
    private final ReadingManagerRepository readingManagerRepository;

    public void setData(
            String title,
            String author,
            String publisher,
            String genre,
            LocalDate dateStarted,
            String review) {
        ReadingManager addData = new ReadingManager();

        addData.setTitle(title);
        addData.setAuthor(author);
        addData.setPublisher(publisher);
        addData.setGenre(genre);
        addData.setDateStarted(dateStarted);
        addData.setDateCompleted(LocalDate.now());
        addData.setReview(review);

        readingManagerRepository.save(addData);
    }

    public List<ReadingManagerItem> getList() {
        List<ReadingManager> allData = readingManagerRepository.findAll();

        List<ReadingManagerItem> result = new LinkedList<>();

        for (ReadingManager item : allData) {
            ReadingManagerItem addItem = new ReadingManagerItem();

            addItem.setId(item.getId());
            addItem.setTitle(item.getTitle());
            addItem.setDateCompleted(item.getDateCompleted());

            result.add(addItem);
        }

        return result;
    }

    public ReadingManagerResponse getBook(long id) {
        ReadingManager allData = readingManagerRepository.findById(id).orElseThrow();

        ReadingManagerResponse result = new ReadingManagerResponse();

        result.setId(allData.getId());
        result.setTitle(allData.getTitle());
        result.setAuthor(allData.getAuthor());
        result.setPublisher(allData.getPublisher());
        result.setGenre(allData.getGenre());
        result.setDateStarted(allData.getDateStarted());
        result.setDateCompleted(allData.getDateCompleted());
        result.setReview(allData.getReview());

        return result;
    }
}

package com.ksy.fitnesscenter.service;

import com.ksy.fitnesscenter.entity.Test;
import com.ksy.fitnesscenter.model.TestItem;
import com.ksy.fitnesscenter.model.TestRequest;
import com.ksy.fitnesscenter.model.TestResponse;
import com.ksy.fitnesscenter.repository.TestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TestService {
    private final TestRepository testRepository;

    public void setStudent(TestRequest request) {
        Test addData = new Test();

        addData.setName(request.getName());
        addData.setScore(request.getScore());
        addData.setTestDate(LocalDate.now());
        addData.setAttendance(request.getAttendance());

        testRepository.save(addData);
    }

    public List<TestItem> getStudents() {
        List<TestItem> result = new LinkedList<>();

        List<Test> allData = testRepository.findAll();

        for (Test item : allData) {
            TestItem addItem = new TestItem();

            addItem.setId(item.getId());
            addItem.setName(item.getName());
            addItem.setScore(item.getScore());

            result.add(addItem);
        }

        return result;
    }

    public TestResponse getStudent(long id) {
        Test allData = testRepository.findById(id).orElseThrow();

        TestResponse result = new TestResponse();
        result.setId(allData.getId());
        result.setName(allData.getName());
        result.setScore(allData.getScore());
        result.setNeedRetest((allData.getScore() < 30) || (!allData.getAttendance()) ? "재시험 필요" : "통과");
        result.setTestDate(allData.getTestDate());
        result.setAttendance(allData.getAttendance());

        return result;
    }
}

package com.ksy.fitnesscenter.service;

import com.ksy.fitnesscenter.entity.Member;
import com.ksy.fitnesscenter.model.MemberItem;
import com.ksy.fitnesscenter.model.MemberRequest;
import com.ksy.fitnesscenter.model.MemberResponse;
import com.ksy.fitnesscenter.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public void setMember(MemberRequest request) {
        Member addData = new Member();

        addData.setName(request.getName());
        addData.setAge(request.getAge());
        addData.setCurrentWeight(request.getCurrentWeight());
        addData.setTargetWeight(request.getTargetWeight());
        addData.setPhone(request.getPhone());
        addData.setUsed(request.getUsed());
        addData.setLastDate(LocalDate.now());
        addData.setInjured(request.getInjured());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<MemberItem> result = new LinkedList<>();

        List<Member> allData = memberRepository.findAll();

        for (Member item : allData) {
            MemberItem addItem = new MemberItem();

            addItem.setId(item.getId());
            addItem.setName(item.getName());
            addItem.setAge(item.getAge());
            addItem.setPhone(item.getPhone());
            addItem.setLastDate(item.getLastDate());

            result.add(addItem);
        }

        return result;
    }

    public MemberResponse getMember(long id) { // long id를 받으면 멤버리스펀스 형태로 만들 거야
        Member allData = memberRepository.findById(id).orElseThrow(); // 멤버형태 이름올데이터. 레포지토리야 데이터베이스에서 이 id를 기준으로 관련 자료 찾아와. 없음말구.

        MemberResponse result = new MemberResponse(); // 형태멤버리스펀스 이름결과. 멤버리스펀스 새로운 줄 추가해.

        result.setId(allData.getId());
        result.setName(allData.getName());
        result.setAge(allData.getAge());
        result.setWeight("현재 체중 " + allData.getCurrentWeight() + " / " + "목표 체중 " + allData.getTargetWeight());
        result.setAdvice(allData.getCurrentWeight() > allData.getTargetWeight() ? "감량 필요" : "감량 불필요");
        result.setPhone(allData.getPhone());
        result.setUsed(allData.getUsed());
        result.setLastDate(allData.getLastDate());
        result.setInjured(allData.getInjured());

        return result;
    }
}

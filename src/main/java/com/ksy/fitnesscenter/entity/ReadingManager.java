package com.ksy.fitnesscenter.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class ReadingManager {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 200)
    private String title;

    @Column(nullable = false, length = 100)
    private String author;

    @Column(length = 100)
    private String publisher;

    @Column(length = 20)
    private String genre;

    @Column
    private LocalDate dateStarted;

    @Column(nullable = false)
    private LocalDate dateCompleted;

    @Column(nullable = false, length = 500)
    private String review;
}

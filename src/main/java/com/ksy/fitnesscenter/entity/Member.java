package com.ksy.fitnesscenter.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private Integer age;

    @Column(nullable = false)
    private Integer currentWeight;

    @Column(nullable = false)
    private Integer targetWeight;

    @Column(nullable = false, length = 20)
    private String phone;

    @Column(nullable = false)
    private Integer used;

    @Column(nullable = false)
    private LocalDate lastDate;

    @Column(length = 300)
    private String injured;
}

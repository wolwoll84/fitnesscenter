package com.ksy.fitnesscenter.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private Integer score;

    @Column(nullable = false)
    private LocalDate testDate;

    @Column(nullable = false)
    private Boolean attendance;
}

package com.ksy.fitnesscenter.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberRequest {

    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @NotNull
    private Integer age;

    @NotNull
    private Integer currentWeight;

    @NotNull
    private Integer targetWeight;

    @NotNull
    @Length(min = 7, max = 20)
    private String phone;

    @NotNull
    private Integer used;

    @Length(max = 300)
    private String injured;
}

package com.ksy.fitnesscenter.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class ReadingManagerRequest {
    @NotNull
    @Length(min = 1, max = 200)
    private String title;

    @NotNull
    @Length(min = 1, max = 100)
    private String author;

    @Length(min = 1, max = 100)
    private String publisher;

    @Length(min = 1, max = 20)
    private String genre;

    private LocalDate dateStarted;
    @NotNull
    @Length(min = 1, max = 500)
    private String review;
}

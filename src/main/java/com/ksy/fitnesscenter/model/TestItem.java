package com.ksy.fitnesscenter.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestItem {
    private Long id;
    private String name;
    private Integer score;
}

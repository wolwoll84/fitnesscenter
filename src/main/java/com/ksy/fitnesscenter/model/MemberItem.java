package com.ksy.fitnesscenter.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberItem {
    private Long id;
    private String name;
    private Integer age;
    private String phone;
    private LocalDate lastDate;
}

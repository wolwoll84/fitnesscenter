package com.ksy.fitnesscenter.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class TestResponse {
    private Long id;
    private String name;
    private Integer score;
    private String needRetest;
    private LocalDate testDate;
    private Boolean attendance;
}

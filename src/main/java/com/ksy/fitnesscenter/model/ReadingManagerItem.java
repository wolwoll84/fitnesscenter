package com.ksy.fitnesscenter.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ReadingManagerItem {
    private Long id;
    private String title;
    private LocalDate dateCompleted;
}

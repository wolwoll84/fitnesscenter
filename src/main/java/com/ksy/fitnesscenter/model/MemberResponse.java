package com.ksy.fitnesscenter.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberResponse {
    private Long id;
    private String name;
    private Integer age;
    private String weight;
    private String advice;
    private String phone;
    private Integer used;
    private LocalDate lastDate;
    private String injured;
}

package com.ksy.fitnesscenter.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class TestRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @NotNull
    private Integer score;

    @NotNull
    private Boolean attendance;
}

package com.ksy.fitnesscenter.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ReadingManagerResponse {
    private Long id;
    private String title;
    private String author;
    private String publisher;
    private String genre;
    private LocalDate dateStarted;
    private LocalDate dateCompleted;
    private String review;
}
